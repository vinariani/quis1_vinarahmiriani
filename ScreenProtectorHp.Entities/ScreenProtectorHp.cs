﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ScreenProtectorHp.Entities
{
    public class ScreenProtectorHp
    {
        [Key]
        public Guid ScreenProtectorHPId { set; get; }

        [Required]
        public string Name { set; get; }

        [Required]
        public int StockScreenProtectorHP { set; get; }


    }
}
