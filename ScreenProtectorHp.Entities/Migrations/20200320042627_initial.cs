﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ScreenProtectorHp.Entities.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "screenProtectorHP",
                columns: table => new
                {
                    ScreenProtectorHPId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    StockScreenProtectorHP = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_screenProtectorHP", x => x.ScreenProtectorHPId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "screenProtectorHP");
        }
    }
}
