﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ScreenProtectorHp.Entities
{
    public class ManagementDbContext : DbContext//harus pake using microsoft.entityframework
    {
        public ManagementDbContext(DbContextOptions<ManagementDbContext> option) : base(option)
        {
            //caara buatnya ctor, tab2x untuk  public ManagementDbContext
        }

        public DbSet<ScreenProtectorHp> screenProtectorHps { set; get; }
       

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ScreenProtectorHp>().ToTable("screenProtectorHp");
          
        }

    }
}

