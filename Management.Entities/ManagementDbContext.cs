﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Entities
{
    public class ManagementDbContext : DbContext
    {
        public ManagementDbContext(DbContextOptions<ManagementDbContext> option) : base(option)
        {
            
        }

        public DbSet<ScreenPtotectorHP> screenPtotectorHPs{ set; get; }
        


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ScreenPtotectorHP>().ToTable("screen");
            
        }

    }
}
