﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Management.Entities
{
    public class ScreenPtotectorHP
    {
        [Key]//biar emplyeeid jd primary key
        public Guid ScreenProtectorId { set; get; }

        [Required]
        public string Name { set; get; }
        [Required]
        public int StockScreenProtectorHP { set; get; }


    }
}
