﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Entities
{
    public class ManagementDbContextFactory : IDesignTimeDbContextFactory<ManagementDbContext>//kalo eror, cntl+.
    {

        public ManagementDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ManagementDbContext>();
            builder.UseSqlite("Data Source=reference.db");
            return new ManagementDbContext(builder.Options);
        }
    }
}
