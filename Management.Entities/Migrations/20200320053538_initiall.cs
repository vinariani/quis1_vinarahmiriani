﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Management.Entities.Migrations
{
    public partial class initiall : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "screen",
                columns: table => new
                {
                    ScreenProtectorId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    StockScreenProtectorHP = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_screen", x => x.ScreenProtectorId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "screen");
        }
    }
}
